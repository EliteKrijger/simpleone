import discord
from discord.ext import commands
from aiohttp import request
from datetime import datetime

class Memes(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def meme(self, ctx):
        URL='https://meme-api.herokuapp.com/gimme'

        embed = discord.Embed(
                colour=ctx.author.colour,
                timestamp=datetime.utcnow()
        )
        async with request('GET', URL) as response:
            if response.status == 200:
                data = await response.json()
                embed.set_author(name=(data['title']))
                embed.set_image(url=data['url'])

                await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Memes(bot))