from discord.ext import commands
import random

class Coinflip(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['flip', 'coinflip'])
    async def coin(self, ctx):
        await ctx.send(f'The coin fell on {random.choice(["Heads", "Tails"])}')

def setup(bot):
    bot.add_cog(Coinflip(bot))