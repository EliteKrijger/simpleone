import discord
import random
from discord.ext import commands
from datetime import datetime
from aiohttp import request

class Pets(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    URL='https://some-random-api.ml/img/'

    @commands.command()
    async def pet(self, ctx):
        pets = ['dog', 'cat', 'panda', 'red_panda', 'birb', 'fox', 'koala']
        random.choice(pets)
        pets = random.choice(pets)
        URL = f'https://some-random-api.ml/img/{pets}'

        embed = discord.Embed(
                colour=ctx.author.colour,
                timestamp=datetime.utcnow()
        )
        async with request('GET', URL) as response:
            if response.status == 200:
                data = await response.json()
                embed.set_author(name=f'{pets}')
                embed.set_image(url=data['link'])

                await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Pets(bot))