import discord
from discord.ext import commands
from aiohttp import request

class Jokes(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def joke(self, ctx):
        URL='https://icanhazdadjoke.com/'

        async with request('GET', URL, headers={"Accept": "application/json"}) as response:
            if response.status == 200:
                data = await response.json()
                await ctx.send(data['joke'])

            else:
                await ctx.send(f'API returned a {response.status} status.')

def setup(bot):
    bot.add_cog(Jokes(bot))