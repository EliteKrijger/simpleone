import discord
from discord.ext import commands
from datetime import datetime

class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def help(self, ctx, arg1=None):
        if arg1 == None:
            embed = discord.Embed(
                colour=ctx.author.colour,
                timestamp=datetime.utcnow()
        )

            embed.set_author(name='Help')
            embed.set_footer(text=f'Requested by {ctx.author}', icon_url=ctx.author.avatar_url)

            embed.add_field(name='Misc', value='miscellaneous commands that don\'t belong in the other categories.', inline=False)
            embed.add_field(name='Fun', value='Fun commands. Mostly under heavy development.', inline=False)
            embed.add_field(name='Moderation', value='The normal moderation commands you\'ll see in most bots.', inline=False)
            await ctx.send(embed=embed)

        if arg1 == 'misc':
            embed = discord.Embed(
                colour=ctx.author.colour,
                timestamp=datetime.utcnow()
            )

            embed.set_author(name='Misc')
            embed.set_footer(text=f'Requested by {ctx.author}', icon_url=ctx.author.avatar_url)

            embed.add_field(name='s!ping', value='Gives the ping of the bot.', inline=True)
            embed.add_field(name='s!userinfo', value='Gives information of a member.', inline=True)
            embed.add_field(name='s!poll', value='Creates a poll. Role reactions are in the making.', inline=False)
            embed.add_field(name='s!calculate', value='Calculates the given numbers.', inline=False)
            await ctx.send(embed=embed)

        if arg1 == 'fun':
            embed = discord.Embed(
                colour=ctx.author.colour,
                timestamp=datetime.utcnow()
            )

            embed.set_author(name='Fun')
            embed.set_footer(text=f'Requested by {ctx.author}', icon_url=ctx.author.avatar_url)

            embed.add_field(name='s!coin', value='Flips a coin. You can also use the aliases: flip and coinflip.', inline=False)
            embed.add_field(name='s!joke', value='Gives a random dad joke.', inline=True)
            embed.add_field(name='s!meme', value='Gives a meme.', inline=True)
            embed.add_field(name='s!pet', value='Gives a random pet out of a selection of 7 pets.', inline=False)
            await ctx.send(embed=embed)

        if arg1 == 'moderation':
            embed = discord.Embed(
                colour=ctx.author.colour,
                timestamp=datetime.utcnow()
            )

            embed.set_author(name='Moderation')
            embed.set_footer(text=f'Requested by {ctx.author}', icon_url=ctx.author.avatar_url)

            embed.add_field(name='s!purge', value='Purges the specified amount of messages.', inline=False)
            embed.add_field(name='s!mute', value='Mutes the specified person.', inline=True)
            embed.add_field(name='s!unmute', value='Unmutes the specified person.', inline=True)
            embed.add_field(name='s!kick', value='Kicks the specified person.', inline=False)
            embed.add_field(name='s!ban', value='Bans the specified person.', inline=True)
            embed.add_field(name='s!unban', value='Unbans the specified person. Do not ping them, as that will not work. s!unban Name#Number is the correct usage.', inline=True)
            await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Help(bot))