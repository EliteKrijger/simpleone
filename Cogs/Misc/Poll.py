import discord
from discord.ext import commands
from datetime import datetime

numbers = ("1️⃣", "2⃣", "3⃣", "4⃣", "5⃣",
		   "6⃣", "7⃣", "8⃣", "9⃣", "🔟")

class Poll(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def poll(self, ctx, question, *options):
            if len(options) > 10:
                await ctx.send('You cannot choose more then 10 options.')

            else:
                embed = discord.Embed(title='Poll',
                            description=question,
                            colour=ctx.author.colour,
                            timestamp=datetime.utcnow())
                    
                fields = [('Options', '\n'.join([f'{numbers[idx]} {option}' for idx, option in enumerate(options)]), False),
                        ('Instructions', 'React to give a vote!', False)]

            for name, value, inline in  fields:
                embed.add_field(name=name, value=value, inline=inline)

            message = await ctx.send(embed=embed)

            for emoji in numbers[:len(options)]:
                await message.add_reaction(emoji)

    @poll.error
    async def poll_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send('You need to give arguments in order to create a poll, do you not know how? Check the help command.')
        if isinstance(error, commands.MissingPermissions):
            await ctx.send('You do not have the permission to create a poll.')
    
def setup(bot):
    bot.add_cog(Poll(bot))