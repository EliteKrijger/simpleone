import discord
from discord.ext import commands
from datetime import datetime

class Calculate(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['calc', 'calculator'])
    async def calculate(self, ctx):
        await ctx.send('Please enter in the first number.')
        num1 = await self.bot.wait_for('message', check=lambda message: message.author == ctx.author)
        await ctx.send('Please enter in the second number.')
        num2 = await self.bot.wait_for('message', check=lambda message: message.author == ctx.author)
        await ctx.send('Which method do you want to use? enter \'methods\' for the different methods in which you can calculate.')
        operation = await self.bot.wait_for('message', check=lambda message: message.author == ctx.author)

        if operation.content == 'methods':
            embed = discord.Embed(
                colour=ctx.author.colour,
                timestamp=datetime.utcnow()
        )

            embed.set_author(name='Methods')
            embed.set_footer(text='Hello there.. How is you my day?')

            embed.add_field(name='addition', value='Respond with \'+\'. This adds the numbers together.', inline=False)
            embed.add_field(name='subtract',value='Respond with \'-\'. This subracts the numbers.', inline=False)
            embed.add_field(name='multiplication',value='Respond with \'*\'. This multiplies the numbers.', inline=False)
            embed.add_field(name='division',value='Respond with \'/\'. This divides the numbers.', inline=False)
            await ctx.send(embed=embed)



        elif operation.content == '+':
            answer = int(num1.content) + int(num2.content)
            await ctx.send(f'{answer}')

        elif operation.content == '-':
            answer = int(num1.content) - int(num2.content)
            await ctx.send(f'{answer}')

        elif operation.content == '*':
            answer = int(num1.content) * int(num2.content)
            await ctx.send(f'{answer}')

        elif operation.content == '/':
            answer = int(num1.content) / int(num2.content)
            await ctx.send(f'{answer}')

def setup(bot):
    bot.add_cog(Calculate(bot))