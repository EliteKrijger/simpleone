from discord.ext import commands

class Purge(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def purge(self, ctx, amount : int):
        await ctx.channel.purge(limit=amount + 1)
        await ctx.send(f':white_check_mark: Succesfully cleared ``{amount}``', delete_after=5)

    @purge.error
    async def purge_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send('You need to specify a amount in order to purge.')
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f'You do not have the permissions to purge.')

def setup(bot):
    bot.add_cog(Purge(bot))