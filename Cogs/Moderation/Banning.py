import discord
from discord.ext import commands

class Banning(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, member : discord.Member, *, reason):
        await member.ban(reason = reason)
        await ctx.send(f':white_check_mark: Banned {member.mention}.')

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def unban(self, ctx, *, member):
        banned_users = await ctx.guild.bans()
        member_name, member_discriminator = member.split('#')

        for ban_entry in banned_users:
            user =  ban_entry.user
        
            if (user.name, user.discriminator) == (member_name, member_discriminator):
                await ctx.guild.unban(user)
                await ctx.send(f':white_check_mark: Unbanned {user.mention}')

    @ban.error
    async def ban_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send('You need to specify a member in order to ban them.')
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f'You do not have the permission to ban {member}.')

    @unban.error
    async def unban_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send('You need to specify a member in order to ban  them.')
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f'You do not have the permissions to ban {member}.')

def setup(bot):
    bot.add_cog(Banning(bot))