import discord
from discord.ext import commands

class Muting(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def mute(self, ctx, member : discord.Member):
        role = discord.utils.get(ctx.guild.roles, name='MUTED')
        await member.add_roles(role)
        await ctx.send(f':white_check_mark: Succesfully muted {member}')

    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def unmute(self, ctx, member : discord.Member):
        role = discord.utils.get(ctx.guild.roles, name='MUTED')
        await member.remove_roles(role)
        await ctx.send(f':white_check_mark: Succesfully unmuted {member}')

    @mute.error
    async def mute_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send('You need to specify a member in order to mute them.')
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f'You do not have the permissions to mute {member}.')

    @unmute.error
    async def unmute_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send('You need to specify a member in order to unmute them.')
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f'You do not have the permissions to unmute {member}.')


def setup(bot):
    bot.add_cog(Muting(bot))