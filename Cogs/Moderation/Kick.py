import discord
from discord.ext import commands

class Kick(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, member : discord.Member, *, reason):
        await member.kick(reason=reason)
        await ctx.send(f':white_check_mark: Succesfully kicked {member.mention}.')

    @kick.error
    async def kick_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send('You need to specify a member in order to kick them.')
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f'You do not hav the required permissions to kick {member}.')

def setup(bot):
    bot.add_cog(Kick(bot))