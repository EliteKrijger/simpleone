import discord
from discord.ext import commands
from datetime import datetime

class Botinfo(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['simpleoneinfo', 'simpleinfo', 'infobot'])
    async def botinfo(self, ctx):
        embed = discord.Embed(
                colour=ctx.author.colour,
                timestamp=datetime.utcnow()
        )

        embed.set_author(name='Simpleinfo')
        embed.set_footer(text='"Simpleinfo; info, as simple as possible"')

        embed.add_field(name='Uptime:', value=
        
        )
        embed.add_field(name='Ping:', value=f'``{round(self.bot.latency * 1000)}ms``', inline=True)
        embed.add_field(name='Version:', value='V0.1', inline=False)

def setup(bot):
    bot.add_cog(Botinfo(bot))