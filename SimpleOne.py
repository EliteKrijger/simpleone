import discord
from discord.ext import commands
from config import BOT_TOKEN

bot = commands.Bot(command_prefix = 's!')
bot.remove_command('help')

@bot.event
async def on_ready():
    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name="s!help"))
    print('Bot is ready')

bot.load_extension('Cogs.Help.Help')
bot.load_extension('Cogs.Misc.Poll')
bot.load_extension('Cogs.Misc.Ping')
bot.load_extension('Cogs.Misc.Calculate')
bot.load_extension('Cogs.Misc.Userinfo')
bot.load_extension('Cogs.Misc.Testing')
bot.load_extension('Cogs.Fun.Memes')
bot.load_extension('Cogs.Fun.Jokes')
bot.load_extension('Cogs.Fun.Pets')
bot.load_extension('Cogs.Fun.Coinflip')
bot.load_extension('Cogs.Moderation.Purge')
bot.load_extension('Cogs.Moderation.Muting')
bot.load_extension('Cogs.Moderation.Kick')
bot.load_extension('Cogs.Moderation.Banning')

bot.run(BOT_TOKEN)